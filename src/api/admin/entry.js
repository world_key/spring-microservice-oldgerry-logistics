import request from '@/router/axios'

const baseUserUrl = '/api/user/v1/entry/'

// 获取已认证的企业
export function getFleet () {
  return request({
    url: baseUserUrl + 'getFleet',
    method: 'get'
  })
}

// 申请入职车队
export function inApplication (obj) {
  return request({
    url: baseUserUrl + 'inApplication',
    method: 'put',
    data: obj
  })
}

// 申请离职
export function dimission (obj) {
  return request({
    url: baseUserUrl,
    method: 'post',
    data: obj
  })
}

// 获取申请进度
export function getEntryInfo (userId) {
  return request({
    url: baseUserUrl + 'getEntryInfo/' + userId,
    method: 'get'
  })
}
