import request from '@/router/axios'
import { getRefreshToken } from '@/utils/auth'
const baseAuthenticationUrl = '/api/auth/v1/authentication/'

const basicAuthorization = 'Basic ' + btoa('web_app:spring-microservice-exam-secret')

/**
 * 登录
 * @param tenantCode 公司标识
 * @param identifier 账号
 * @param credential 密码
 * @param code 验证码
 * @param randomStr 随机数
 */
export function loginByUsername (tenantCode, identifier, credential, code, randomStr) {
  const grantType = 'password'
  const scope = 'read'
  return request({
    url: '/api/auth/oauth/token',
    headers: {
      'Authorization': basicAuthorization,
      'Tenant-Code': tenantCode
    },
    method: 'post',
    params: { username: identifier, credential, randomStr, code, grant_type: grantType, scope }
  })
}

// 效验手机号
export function verifyPhone (phone) {
  return request({
    url: '/api/user/v1/user/verifyPhone',
    method: 'post',
    params: { phone }
  })
}

// 获取验证码
export function getCode (phone) {
  return request({
    url: '/api/user/v1/user/getCode',
    method: 'post',
    params: { phone }
  })
}

// 注册
export function register (obj) {
  return request({
    url: '/api/user/v1/user/register',
    headers: {
      'Authorization': basicAuthorization,
      'Tenant-Code': 'jxwl' // 默认先给个jxwl 后期再改动
    },
    method: 'post',
    params: { phone: obj.phone, code: obj.code, credential: obj.credential }
  })
}

export function logout (accesstoken, refreshToken) {
  return request({
    url: baseAuthenticationUrl + 'removeToken',
    method: 'post'
  })
}

export function getUserInfo (token) {
  return request({
    url: '/api/user/v1/user/info',
    method: 'get'
  })
}

/**
 * 刷新token
 */
export function refreshToken () {
  //  grant_type为refresh_token
  const grantType = 'refresh_token'
  const scope = 'read'
  const refreshToken = getRefreshToken()
  return request({
    url: '/api/auth/oauth/token',
    headers: {
      'Authorization': basicAuthorization
    },
    method: 'post',
    params: { grant_type: grantType, scope, refresh_token: refreshToken }
  })
}
