import request from '@/router/axios'

const baseUserUrl = '/api/user/v1/user/'

export function fetchList (query) {
  return request({
    url: baseUserUrl + 'userList',
    method: 'get',
    params: query
  })
}

export function addObj (obj) {
  return request({
    url: baseUserUrl,
    method: 'post',
    data: obj
  })
}

export function getObj (id) {
  return request({
    url: baseUserUrl + id,
    method: 'get'
  })
}

export function delObj (id) {
  return request({
    url: baseUserUrl + id,
    method: 'delete'
  })
}

export function putObj (obj) {
  return request({
    url: baseUserUrl,
    method: 'put',
    data: obj
  })
}

export function updateObjInfo (obj) {
  return request({
    url: baseUserUrl + 'updateInfo',
    method: 'put',
    data: obj
  })
}

// 新增认证信息
export function sjAuth (obj) {
  return request({
    url: baseUserUrl + 'sjAuth',
    method: 'put',
    data: obj
  })
}

// 获取认证信息
export function getAuthInfo (query) {
  return request({
    url: baseUserUrl + 'getAuthInfo',
    method: 'get',
    params: query
  })
}

// 查询企业代码是否重复

export function seleteRepetition (corpId) {
  return request({
    url: baseUserUrl + 'seleteRepetition/' + corpId,
    method: 'get'
  })
}

// 审核认证信息
export function auditAuth (obj) {
  return request({
    url: baseUserUrl + 'auditAuth',
    method: 'put',
    params: obj
  })
}

export function updatePassword (obj) {
  return request({
    url: baseUserUrl + 'updatePassword',
    method: 'put',
    data: obj
  })
}

export function bindEmail (obj) {
  return request({
    url: baseUserUrl + 'bindEmail',
    method: 'put',
    data: obj
  })
}

export function updateAvatar (obj) {
  return request({
    url: baseUserUrl + 'updateAvatar',
    method: 'put',
    data: obj
  })
}

export function delAllObj (obj) {
  return request({
    url: baseUserUrl + 'deleteAll',
    method: 'post',
    data: obj
  })
}

// 导出
export function exportObj (obj) {
  return request({
    url: baseUserUrl + 'export',
    method: 'post',
    responseType: 'arraybuffer',
    headers: { 'filename': 'utf-8' },
    data: obj
  })
}

// 重置密码
export function resetPassword (obj) {
  return request({
    url: baseUserUrl + 'resetPassword',
    method: 'put',
    data: obj
  })
}

// 获取短信验证码
export function getPhoneCode (obj) {
  return request({
    url: baseUserUrl + 'getPhoneCode',
    method: 'post',
    params: obj
  })
}
// 修改手机身份验证
export function updatePhoneValid (obj) {
  return request({
    url: baseUserUrl + 'updatePhoneValid',
    method: 'post',
    params: obj
  })
}

export function bindPhone (obj) {
  return request({
    url: baseUserUrl + 'bindPhone',
    method: 'put',
    data: obj
  })
}
