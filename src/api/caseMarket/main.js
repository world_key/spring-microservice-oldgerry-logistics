import request from '@/router/axios'

const baseCaseMarketUrl = '/api/logistics/v1/caseMarket/'

export function caseMarketList () {
  return request({
    url: baseCaseMarketUrl + 'caseMarketList',
    method: 'get'
  })
}

export function fetchList (query) {
  return request({
    url: baseCaseMarketUrl + 'caseMarketList',
    method: 'get',
    params: query
  })
}

export function fetchListByLimit (query) {
  return request({
    url: baseCaseMarketUrl + 'caseMarketListByLimit',
    method: 'get',
    params: query
  })
}

export function getObj (id) {
  return request({
    url: baseCaseMarketUrl + id,
    method: 'get'
  })
}

export function preview (id) {
  return request({
    url: baseCaseMarketUrl + id + '/preview',
    method: 'get'
  })
}

export function addObj (obj) {
  return request({
    url: baseCaseMarketUrl,
    method: 'post',
    data: obj
  })
}

export function putObj (obj) {
  return request({
    url: baseCaseMarketUrl,
    method: 'put',
    data: obj
  })
}

export function delObj (id) {
  return request({
    url: baseCaseMarketUrl + id,
    method: 'delete'
  })
}

export function delAllObj (obj) {
  return request({
    url: baseCaseMarketUrl + 'deleteAll',
    method: 'post',
    data: obj
  })
}

export function getDictByType (query) {
  return request({
    url: '/api/user/v1/user/' + 'getDictByType',
    method: 'get',
    params: query
  })
}
