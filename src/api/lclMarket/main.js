import request from '@/router/axios'

const baseLclMarketUrl = '/api/logistics/v1/lclMarket/'

export function lclMarketList () {
  return request({
    url: baseLclMarketUrl + 'lclMarketList',
    method: 'get'
  })
}

export function fetchList (query) {
  return request({
    url: baseLclMarketUrl + 'lclMarketList',
    method: 'get',
    params: query
  })
}

export function fetchListByLimit (query) {
  return request({
    url: baseLclMarketUrl + 'lclMarketListByLimit',
    method: 'get',
    params: query
  })
}

export function getObj (id) {
  return request({
    url: baseLclMarketUrl + id,
    method: 'get'
  })
}

export function preview (id) {
  return request({
    url: baseLclMarketUrl + id + '/preview',
    method: 'get'
  })
}

export function addObj (obj) {
  console.info(obj)
  return request({
    url: baseLclMarketUrl,
    method: 'post',
    data: obj
  })
}

export function putObj (obj) {
  return request({
    url: baseLclMarketUrl,
    method: 'put',
    data: obj
  })
}

export function delObj (id) {
  return request({
    url: baseLclMarketUrl + id,
    method: 'delete'
  })
}

export function delAllObj (obj) {
  return request({
    url: baseLclMarketUrl + 'deleteAll',
    method: 'post',
    data: obj
  })
}

export function getDictByType (query) {
  return request({
    url: '/api/user/v1/user/' + 'getDictByType',
    method: 'get',
    params: query
  })
}
