import request from '@/router/axios'

const baseUserUrl = '/api/logistics/v1/clientele/'

// 获取客户清单列表
export function fetchList (query) {
  return request({
    url: baseUserUrl + 'clienteleList',
    method: 'get',
    params: query
  })
}

// 获取充值总额、消费总额、结余总额
export function getDriver (creator) {
  return request({
    url: baseUserUrl + 'getDriver/' + creator,
    method: 'get'
  })
}

// 获取我的账单(接单员)
export function driverBillList (query) {
  return request({
    url: baseUserUrl + 'driverBillList',
    method: 'get',
    params: query
  })
}
