import request from '@/router/axios'

const baseUserUrl = '/api/logistics/v1/packing/'

// 获取尺寸类型
export function getDimensionsType () {
  return request({
    url: baseUserUrl + 'dimensionsTypeList',
    method: 'get'
  })
}

// 获取箱经营人代码
export function getBoxOperaor () {
  return request({
    url: baseUserUrl + 'boxOperatorList',
    method: 'get'
  })
}

// 获取海关代码
export function getCustoms () {
  return request({
    url: baseUserUrl + 'customsList',
    method: 'get'
  })
}

// 添加装箱单
export function addPacking (obj) {
  return request({
    url: baseUserUrl + 'addPacking',
    method: 'post',
    data: obj
  })
}

// 获取装箱单列表
export function fetchList (query) {
  return request({
    url: baseUserUrl + 'packingList',
    method: 'get',
    params: query
  })
}

// 查询是否可接单
export function toOrder (orderTaker) {
  return request({
    url: baseUserUrl + 'toOrder/' + orderTaker,
    method: 'get'
  })
}

// 接单
export function takeOrdersObj (obj) {
  return request({
    url: baseUserUrl + 'takeOrders',
    method: 'post',
    data: obj
  })
}

// 审核
export function auditObj (obj) {
  return request({
    url: baseUserUrl + 'audit',
    method: 'post',
    data: obj
  })
}

// 获取提单号
export function getBlnoList (uuid) {
  return request({
    url: baseUserUrl + 'getBlnoList/' + uuid,
    method: 'get'
  })
}

// 接单员发送
export function sendPacking (obj) {
  return request({
    url: baseUserUrl + 'sendPacking',
    method: 'post',
    data: obj
  })
}

// 修改装箱单
export function editPacking (obj) {
  return request({
    url: baseUserUrl + 'editPacking',
    method: 'post',
    data: obj
  })
}

// 获取司机建单记录列表
export function driverSingleList (query) {
  return request({
    url: baseUserUrl + 'driverSingleList',
    method: 'get',
    params: query
  })
}

// 申请重新发送
export function requestResend (obj) {
  return request({
    url: baseUserUrl + 'requestResend',
    method: 'post',
    data: obj
  })
}

// 获取消费金额
export function getPackById (billNo) {
  return request({
    url: baseUserUrl + 'getPackById/' + billNo,
    method: 'get'
  })
}

// 获取重发审核列表
export function resendAuditList (query) {
  return request({
    url: baseUserUrl + 'resendAuditList',
    method: 'get',
    params: query
  })
}

// 重发审核不通过
export function auditNoPass (obj) {
  return request({
    url: baseUserUrl + 'auditNoPass',
    method: 'post',
    data: obj
  })
}

// 重发审核通过
export function auditPass (obj) {
  return request({
    url: baseUserUrl + 'auditPass',
    method: 'post',
    data: obj
  })
}

// 获取Excel文件效验列表
export function getExcleList (idString) {
  return request({
    url: baseUserUrl + 'getExcleList/' + idString,
    method: 'get'
  })
}

// 进行效验
export function efficacyExcle (obj) {
  return request({
    url: baseUserUrl + 'efficacyExcle',
    method: 'post',
    data: obj
  })
}

// 平台发送
export function terraceSend (obj) {
  return request({
    url: baseUserUrl + 'terraceSend',
    method: 'post',
    data: obj
  })
}

// 发送并导出Excel
export function getExcle (obj) {
  return request({
    url: baseUserUrl + 'getExcle',
    method: 'post',
    data: obj,
    responseType: 'blob'
  })
}
