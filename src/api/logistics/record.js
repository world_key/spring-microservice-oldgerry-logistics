import request from '@/router/axios'

const baseUserUrl = '/api/logistics/v1/record/'

// 获取接单员清单列表
export function fetchList (query) {
  return request({
    url: baseUserUrl + 'recordList',
    method: 'get',
    params: query
  })
}

// 发送记录
export function inviaOrderList (query) {
  return request({
    url: baseUserUrl + 'inviaOrderList',
    method: 'get',
    params: query
  })
}

// 获取我的账单(接单员)
export function myBillList (query) {
  return request({
    url: baseUserUrl + 'myBillList',
    method: 'get',
    params: query
  })
}

// 获取支出收入
export function getInAndEx (creator) {
  return request({
    url: baseUserUrl + 'getInAndEx/' + creator,
    method: 'get'
  })
}
