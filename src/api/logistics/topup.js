import request from '@/router/axios'

const baseUserUrl = '/api/logistics/v1/topup/'

// 获取装箱单列表
export function fetchList (query) {
  return request({
    url: baseUserUrl + 'TopupList',
    method: 'get',
    params: query
  })
}
