import request from '@/router/axios'

const baseUserUrl = '/api/motorcade/mc/approval/'

// 获取入职申请列表
export function getEntry (query) {
  return request({
    url: baseUserUrl + 'getEntry',
    method: 'get',
    params: query
  })
}

// 审批入职
export function verdict (obj) {
  return request({
    url: baseUserUrl + 'verdict',
    method: 'put',
    data: obj
  })
}
