import request from '@/router/axios'

const baseDeptUrl = '/api/motorcade/mc/dept/'

// 获取部门列表树
export function fetchTree (query) {
  return request({
    url: baseDeptUrl + 'depts',
    method: 'get',
    params: query
  })
}

// 获取当前部门信息
export function getObj (id) {
  return request({
    url: baseDeptUrl + id,
    method: 'get'
  })
}

// 删除当前部门
export function delObj (id) {
  return request({
    url: baseDeptUrl + id,
    method: 'delete'
  })
}

// 修改当前部门
export function putObj (obj) {
  return request({
    url: baseDeptUrl,
    method: 'put',
    data: obj
  })
}

// 添加新部门
export function addObj (obj) {
  return request({
    url: baseDeptUrl,
    method: 'post',
    data: obj
  })
}
