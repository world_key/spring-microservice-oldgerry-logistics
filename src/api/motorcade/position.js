import request from '@/router/axios'

const baseRoleUrl = '/api/motorcade/mc/position/'

// 获取职位列表
export function fetchList (query) {
  return request({
    url: baseRoleUrl + 'positionList',
    method: 'get',
    params: query
  })
}

// 创建职位
export function addObj (obj) {
  return request({
    url: baseRoleUrl,
    method: 'post',
    data: obj
  })
}

// 根据id删除职位
export function delObj (id) {
  return request({
    url: baseRoleUrl + id,
    method: 'delete'
  })
}

// 根据职位id批量删除职位
export function delAllObj (obj) {
  return request({
    url: baseRoleUrl + 'deleteAll',
    method: 'post',
    data: obj
  })
}

// 根据职位id更新职位的基本信息
export function putObj (obj) {
  return request({
    url: baseRoleUrl,
    method: 'put',
    data: obj
  })
}

// 获取全部职位列表
export function allPositions (query) {
  return request({
    url: baseRoleUrl + 'allPositions',
    method: 'get',
    params: query
  })
}

// 查询车队的权限列表
export function getMenuTree (roleCode) {
  return request({
    url: '/api/user/v1/menu/getMenuTree/' + roleCode,
    method: 'get'
  })
}

// 查询司机原有权限
export function RoleTree (roleName) {
  return request({
    url: '/api/user/v1/menu/roleTree/' + roleName,
    method: 'get'
  })
}

// 根据职位查找菜单
export function fetchRoleTree (positionCode) {
  return request({
    url: baseRoleUrl + 'positionTree/' + positionCode,
    method: 'get'
  })
}

// 更新职位菜单
export function permissionUpdate (id, menus) {
  return request({
    url: baseRoleUrl + 'positionMenuUpdate',
    method: 'put',
    data: {
      id: id,
      menuIds: menus
    }
  })
}
