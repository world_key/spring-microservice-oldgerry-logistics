import request from '@/router/axios'

const baseStaffUrl = '/api/motorcade/mc/staff/'

// 获取员工列表
export function fetchList (query) {
  return request({
    url: baseStaffUrl + 'staffList',
    method: 'get',
    params: query
  })
}

// 修改员工信息
export function putObj (obj) {
  return request({
    url: baseStaffUrl,
    method: 'put',
    data: obj
  })
}

// 开除员工
export function delObj (id) {
  return request({
    url: baseStaffUrl + id,
    method: 'delete'
  })
}
