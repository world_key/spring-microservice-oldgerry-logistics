import request from '@/router/axios'

const baseUserUrl = '/api/msc/v1/mail/'

// 获取支出收入
export function sendMail (query) {
  return request({
    url: baseUserUrl + 'sendMail',
    method: 'get',
    params: query
  })
}
