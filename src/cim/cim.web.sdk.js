import protoRoot from '@/proto/proto'
// import protobuf from 'protobufjs'

/* CIM服务器IP */
let CIM_HOST = '127.0.0.1'
/* CIM服务端口 */
let CIM_PORT = 23456
let CIM_URI = 'ws://' + CIM_HOST + ':' + CIM_PORT

let CMD_HEARTBEAT_RESPONSE = new Uint8Array([67, 82])
let SDK_VERSION = '1.0.0'
let SDK_CHANNEL = 'browser'
let APP_PACKAGE = 'com.farsunset.cim'
let ACTION_999 = '999'// 特殊的消息类型，代表被服务端强制下线
let DATA_HEADER_LENGTH = 3

let C_H_RS = 0
let S_H_RQ = 1
let MESSAGE = 2
let SENTBODY = 3
let REPLYBODY = 4

let socket
let manualStop = false
let CIMPushManager = {}
CIMPushManager.connection = function () {
  manualStop = false
  window.localStorage.account = ''
  socket = new WebSocket(CIM_URI)
  socket.cookieEnabled = false
  socket.binaryType = 'arraybuffer'
  socket.onopen = CIMPushManager.innerOnConnectionSuccessed
  socket.onmessage = CIMPushManager.innerOnMessageReceived
  socket.onclose = CIMPushManager.innerOnConnectionClosed
}

CIMPushManager.bindAccount = function (account) {
  window.localStorage.account = account

  let deviceId = window.localStorage.deviceIddeviceId
  if (deviceId === '' || deviceId === undefined) {
    deviceId = generateUUID()
    window.localStorage.deviceId = deviceId
  }

  let browser = getBrowser()
  let body = protoRoot.lookup('com.farsunset.cim.sdk.web.model.SentBody')
  body.setKey('client_bind')
  body.getDataMap().set('account', account)
  body.getDataMap().set('channel', SDK_CHANNEL)
  body.getDataMap().set('version', SDK_VERSION)
  body.getDataMap().set('osVersion', browser.version)
  body.getDataMap().set('packageName', APP_PACKAGE)
  body.getDataMap().set('deviceId', deviceId)
  body.getDataMap().set('device', browser.name)
  CIMPushManager.sendRequest(body)
}

CIMPushManager.stop = function () {
  manualStop = true
  socket.close()
}

CIMPushManager.resume = function () {
  manualStop = false
  CIMPushManager.connection()
}

CIMPushManager.onConnectionSuccessed = function () {}
CIMPushManager.onReplyReceived = function () {}
CIMPushManager.onMessageReceived = function () {}

CIMPushManager.innerOnConnectionSuccessed = function () {
  let account = window.localStorage.account
  if (account === '' || account === undefined) {
    CIMPushManager.onConnectionSuccessed()
  } else {
    CIMPushManager.bindAccount(account)
  }
}

CIMPushManager.innerOnMessageReceived = function (e) {
  let data = new Uint8Array(e.data)

  let type = data[0]
  let length = getContentLength(data[1], data[2])
  /**
   * 收到服务端发来的心跳请求，立即回复响应，否则服务端会在10秒后断开连接
   */
  if (type === S_H_RQ) {
    CIMPushManager.sendHeartbeatResponse()
    return
  }

  if (type === MESSAGE) {
    data = protoRoot.lookup('com.farsunset.cim.sdk.web.model.Message').deserializeBinary(data.subarray(DATA_HEADER_LENGTH, DATA_HEADER_LENGTH + length))
    onInterceptMessageReceived(data.toObject(false))
    return
  }

  if (type === REPLYBODY) {
    data = protoRoot.lookup('com.farsunset.cim.sdk.web.model.ReplyBody').deserializeBinary(data.subarray(DATA_HEADER_LENGTH, DATA_HEADER_LENGTH + length))
    /**
     * 将proto对象转换成json对象，去除无用信息
     */
    let reply = {}
    reply.code = data.getCode()
    reply.key = data.getKey()
    reply.message = data.getMessage()
    reply.timestamp = data.getTimestamp()
    reply.data = {}

    /**
     * 注意，遍历map这里的参数 value在前key在后
     */
    data.getDataMap().forEach(function (v, k) {
      reply.data[k] = v
    })
    CIMPushManager.onReplyReceived()
  }
}

CIMPushManager.innerOnConnectionClosed = function (e) {
  if (!manualStop) {
    let time = Math.floor(Math.random() * (30 - 15 + 1) + 15)
    setTimeout(function () {
      CIMPushManager.connection()
    }, time)
  }
}

CIMPushManager.sendRequest = function (body) {
  let data = body.serializeBinary()
  let header = buildHeader(SENTBODY, data.length)
  let protubuf = new Uint8Array(data.length + header.length)
  protubuf.set(header, 0)
  protubuf.set(data, header.length)
  socket.send(protubuf)
}

CIMPushManager.sendHeartbeatResponse = function () {
  let data = CMD_HEARTBEAT_RESPONSE
  let header = buildHeader(C_H_RS, data.length)
  let protubuf = new Uint8Array(data.length + header.length)
  protubuf.set(header, 0)
  protubuf.set(data, header.length)
  socket.send(protubuf)
}

function getContentLength (lv, hv) {
  let l = (lv & 0xff)
  let h = (hv & 0xff)
  return (l | (h <<= 8))
}

function buildHeader (type, length) {
  let header = new Uint8Array(DATA_HEADER_LENGTH)
  header[0] = type
  header[1] = (length & 0xff)
  header[2] = ((length >> 8) & 0xff)
  return header
}

function onInterceptMessageReceived (message) {
  // 被强制下线之后，不再继续连接服务端
  if (message.action === ACTION_999) {
    manualStop = true
  }
  // 收到消息后，将消息发送给页面
  if (CIMPushManager.onReplyReceived instanceof Function) {
    CIMPushManager.onReplyReceived()
  }
}

function getBrowser () {
  let explorer = window.navigator.userAgent.toLowerCase()
  // ie
  if (explorer.indexOf('msie') >= 0) {
    let ver = explorer.match(/msie ([\d.]+)/)[1]
    return { name: 'IE', version: ver }
  }
  // firefox
  else if (explorer.indexOf('firefox') >= 0) {
    let ver = explorer.match(/firefox\/([\d.]+)/)[1]
    return { name: 'Firefox', version: ver }
  }
  // Chrome
  else if (explorer.indexOf('chrome') >= 0) {
    let ver = explorer.match(/chrome\/([\d.]+)/)[1]
    return { name: 'Chrome', version: ver }
  }
  // Opera
  else if (explorer.indexOf('opera') >= 0) {
    let ver = explorer.match(/opera.([\d.]+)/)[1]
    return { name: 'Opera', version: ver }
  }
  // Safari
  else if (explorer.indexOf('Safari') >= 0) {
    let ver = explorer.match(/version\/([\d.]+)/)[1]
    return { name: 'Safari', version: ver }
  }

  return { name: 'Other', version: '1.0.0' }
}

function generateUUID () {
  let d = new Date().getTime()
  let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16)
  })
  return uuid.replace(/-/g, '')
}
