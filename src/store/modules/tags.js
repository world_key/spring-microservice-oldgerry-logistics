import { getStore, removeStore, setStore } from '@/utils/store'

const tagObj = {
  label: '',
  value: '',
  query: '',
  num: '',
  close: true
}
// 若list的长度等于1，那么list的第一个复制为false,不为1就查找list数组中close的值是否为true
function setFistTag (list) {
  if (list.length === 1) {
    list[0].close = false
  } else {
    list.some(a => {
      a.close = true
    })
  }
  return list
}

const navs = {
  // state全局通用data属性
  state: {
    tagList: getStore({ name: 'tagList' }) || [],
    tag: getStore({ name: 'tag' }) || tagObj,
    tagWel: {
      label: '首页',
      value: '/dashboard'
    },
    tagCurrent: getStore({ name: 'tagCurrent' }) || []
  },
  actions: {},
  mutations: {
    ADD_TAG: (state, action) => {
      state.tag = action
      // 存入sessionStorage
      setStore({ name: 'tag', content: state.tag })
      // 如果tagList中a的值等于action的值就返回 添加tagList数组的新元素
      if (state.tagList.some(a => a.value === action.value)) return
      state.tagList.push({
        label: action.label,
        value: action.value,
        query: action.query
      })
      // 判断后重新赋值
      state.tagList = setFistTag(state.tagList)
      // 存入将taglist也存入sessionStorage
      setStore({ name: 'tagList', content: state.tagList })
    },
    SET_TAG_CURRENT: (state, tagCurrent) => {
      state.tagCurrent = tagCurrent
      setStore({ name: 'tagCurrent', content: state.tagCurrent })
    },
    SET_TAG: (state, value) => {
      state.tagList.forEach((ele, num) => {
        if (ele.value === value) {
          state.tag = state.tagList[num]
          setStore({ name: 'tag', content: state.tag })
        }
      })
    },
    DEL_ALL_TAG: (state, action) => {
      state.tag = tagObj
      state.tagList = []
      state.tagList.push(state.tagWel)
      removeStore({ name: 'tag' })
      removeStore({ name: 'tagList' })
    },
    DEL_TAG_OTHER: (state, action) => {
      state.tagList.forEach((ele, num) => {
        if (ele.value === state.tag.value) {
          state.tagList = state.tagList.slice(num, num + 1)
          state.tag = state.tagList[0]
          state.tagList[0].close = false
          setStore({ name: 'tag', content: state.tag })
          setStore({ name: 'tagList', content: state.tagList })
        }
      })
    },
    DEL_TAG: (state, action) => {
      state.tagList.forEach((ele, num) => {
        if (ele.value === action.value) {
          state.tagList.splice(num, 1)
          state.tagList = setFistTag(state.tagList)
          setStore({ name: 'tag', content: state.tag })
          setStore({ name: 'tagList', content: state.tagList })
        }
      })
    }
  }
}
export default navs
